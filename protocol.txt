Game Demo Protocol

Packets from Client
==========================

JOIN - sent when joining game

    O   L   Datatype    Desc
    ---------------------------------------------------------
    0   4   ascii       "JOIN"
    4	1   uint8	username length
    5	?   ascii	username

INPT - current state of the client's input

    O   L   Datatype    Desc
    ---------------------------------------------------------
    0   4   ascii       "INPT"
    4	1   int8	direction of horizontal input
    5	1   int8	direction of vertical input

Packets from Server
==========================

REPL - this is sent when creating/updating/deleting game objects

    O   L   Datatype    Desc
    ---------------------------------------------------------
    0   4   ascii       "REPL"
    4   1   uint8  	1/2/3 (create/update/delete)

    5	4   ascii       class ID (networkID for delete)
    9   ?   serialize

PAWN - tells the client what pawn they control

    O   L   Datatype    Desc
    ---------------------------------------------------------
    0   4   ascii       "PAWN"
    4	1   uint8	networkID

HOST - Periodically sends over networks to broadcast server IP and port

    O   L   Datatype    Desc
    ---------------------------------------------------------
    0   4   ascii       "HOST"
    4	2   uint16	port of our server
    6   1   uint8 	length of server name
    7   ?   ascii	name of the server

JOIN - response to client's JOIN packet

    O   L   Datatype    Desc
    ---------------------------------------------------------
    0   4   ascii       "JOIN"
    4	1   uint8	join response

    (accepted:) 
    1: player 
    2: spectator
    (denied:)
    3: username too long
    4: username too short
    5: username has invalid characters
    6: username already taken
    7: username not allowed (profanity)
    8: game is full