const { NetworkObject } = require("./class-networkObject");


exports.Bead = class Bead extends NetworkObject {

    constructor() {
        super();

        this.classID = "BEAD";
        this.pos = {
            x: Math.random() * 60 - 30,
            y: Math.random() * 60 - 30,
            z: 0
        }

        this.scale = {
            x:.2,
            y:.2,
            z:.2
        }
        this.radius = this.scale.x/2;
        this.color.r = Math.floor(Math.random() * 255);
        this.color.g = Math.floor(Math.random() * 255);
        this.color.b = Math.floor(Math.random() * 255);
    }

    serialize() {
        let b = super.serialize();

        return b;
    }
}