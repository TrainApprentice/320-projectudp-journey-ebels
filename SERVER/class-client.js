const {Game} = require("./class-game");
const { Pawn } = require("./class-pawn");

exports.Client = class Client {

    static TIMEOUT = 15;

    constructor(rinfo, name) {

    
        this.rinfo = rinfo;

        this.input = {
            x: 0,
            y: 0,
            z: 0
        };
        this.pawn = null;
        this.username = "";
        this.timeOfLastPacket = Game.Singleton.time;

    }

    update() {
        const game = Game.Singleton;

        if(game.time > this.timeOfLastPacket + Client.TIMEOUT) {
            game.server.disconnectClient(this);
        }
    }

    spawnPawn(game) {
        if(this.pawn) return;

        this.pawn = new Pawn();
        game.spawnObject(this.pawn);
    }

    onPacket(packet, game) {
        if(packet.length < 4) return;

        const packetID = packet.slice(0, 4).toString();


        this.timeOfLastPacket = game.time;

        switch(packetID) {
            case "INPT":
                if(packet.length < 6) return;
                
                this.input.x = packet.readInt8(4);
                this.input.y = packet.readInt8(5);

                if(this.pawn) this.pawn.input = this.input;
                
                break;
            default:
                console.log("Error: unknown packet ID");
                break;
        }

    }
}