const { Bead } = require("./class-bead");


exports.Game = class Game {

    static Singleton;

    constructor(server) {

        Game.Singleton = this;

        this.frame = 0;
        this.time = 0;
        this.dt = 16/1000;

        this.timeUntilNextStatePacket = .1;
        this.objs = [];
        this.server = server;
        this.update();
    }

    update() {
        this.time += this.dt;
        this.frame++;

        this.server.update(this);

        //if(player) {}
        let beadCount = 0;
        for(var i in this.objs) {
            this.objs[i].update(this);
            if(this.objs[i].classID == "BEAD") beadCount++;

            if(this.objs[i].classID == "PAWN") this.checkCollision(this.objs[i]);

            if(this.objs[i].isDead) this.deleteObj(this.objs[i]);
        }

        if(beadCount < 30) this.randomBeads(60-beadCount);

        if(this.timeUntilNextStatePacket < .1) {
            this.timeUntilNextStatePacket += this.dt;
        }
        else {
            this.timeUntilNextStatePacket -= .1;
            
            this.sendWorldState();

        }

        setTimeout(() => this.update(), this.dt * 1000);
    }

    sendWorldState() {
        const packet = this.makeREPL();
        this.server.sendPacketToAll(packet);
    }
    makeREPL(isUpdate = true) {
        isUpdate = !!isUpdate;

        let packet = Buffer.alloc(5);
        packet.write("REPL");
        packet.writeUInt8(isUpdate ? 2 : 1, 4);

        this.objs.forEach(o => {
            const classID = Buffer.from(o.classID);
            const data = o.serialize();

            packet = Buffer.concat([packet, classID, data]);
        });

        return packet;
    }
    randomBeads(count) {
        console.log(count);
        for(let i = 0; i < count; i++) {
            let b = new Bead();
            this.spawnObject(b);
        }
    }

    spawnObject(obj) {
        this.objs.push(obj);

        let packet = Buffer.alloc(5);

        packet.write("REPL", 0);
        packet.writeUInt8(1, 4);

        const classID = Buffer.from(obj.classID);
        const data = obj.serialize();

        packet = Buffer.concat([packet, classID, data]);

        this.server.sendPacketToAll(packet);

    }
    deleteObj(obj) {
        const index = this.objs.indexOf(obj);
        if(index < 0) return;

        const netID = this.objs[index].networkID;

        this.objs.splice(index, 1);

        const packet = Buffer.alloc(6);

        packet.write("REPL", 0);
        packet.writeUint8(3, 4);
        packet.writeUint8(netID, 5);

        this.server.sendPacketToAll(packet);
    }
    checkCollision(obj) {
        for(let i in this.objs) {
            if(this.objs[i] != obj) {
                let dx = this.objs[i].pos.x - obj.pos.x;
                let dy = this.objs[i].pos.y - obj.pos.y;
                let dist = Math.sqrt(dx * dx + dy * dy);

                if(dist < this.objs[i].radius + obj.radius){
                    if(this.objs[i].classID == "BEAD") {
                        this.objs[i].isDead = true;
                        obj.consume("BEAD");
                    }
                    if(this.objs[i].classID == "PAWN") {
                        if(obj.scale.x > this.objs[i].scale.x * 1.3) {
                            this.objs[i].isDead = true;
                            obj.consume("PAWN", this.objs[i].scale.x);
                        } 
                        if(this.objs[i].scale.x > obj.scale.x * 1.3){
                            obj.isDead = true;
                            this.objs[i].consume("PAWN", obj.scale.x);
                        }
                    }
                }

                
            }
        }
    }
}