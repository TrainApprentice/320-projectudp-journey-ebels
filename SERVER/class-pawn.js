const { NetworkObject } = require("./class-networkObject");

exports.Pawn = class Pawn extends NetworkObject {
    constructor() {
        super();

        this.classID = "PAWN";
        this.input = {};
        this.velocity = {x:0, y:0, z:0};
        this.speed = 5;
        
        this.color.r = Math.floor(Math.random() * 255);
        this.color.g = Math.floor(Math.random() * 255);
        this.color.b = Math.floor(Math.random() * 255);

    }

    accelerate(vel, acc, dt) {
        if(acc) {
            vel += acc * dt;
            
        } else {
            if(vel > 0) {
                acc = -1;
                vel += acc * dt;
                if(vel < 0) vel = 0;
            }
            if(vel < 0) {
                acc = 1;
                vel += acc * dt;
                if(vel > 0) vel = 0;
            }
        }

        return vel ? vel : 0;
    }

    update(game) {
        super.update(game);
        let moveX = (this.input.x * this.speed) | 0;
        let moveY = (this.input.y * this.speed) | 0;

        
        this.pos.x += moveX * game.dt;
        this.pos.y += moveY * game.dt;

    }

    serialize() {
        let b = super.serialize();

        return b;
    }

    consume(type, num = 0) {
        switch(type) {
            case "BEAD":
                this.scale.x += .1;
                this.scale.y += .1;
                this.scale.z += .1;
                break;
            case "PAWN":
                this.scale.x += num/4;
                this.scale.y += num/4;
                this.scale.z += num/4;
                break;
        }
    }
}