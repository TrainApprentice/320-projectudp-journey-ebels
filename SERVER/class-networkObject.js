

exports.NetworkObject = class NetworkObject {

    static _idCount = 0;
    constructor() {
        this.classID = "NWOB";
        this.networkID = ++NetworkObject._idCount;

        this.pos = {
            x:0,
            y:0,
            z:0
        };
        this.rotation = {
            x:0,
            y:0,
            z:0
        };
        this.scale = {
            x:1,
            y:1,
            z:1
        };
        this.color = {
            r: 0,
            g: 0,
            b: 0
        }
        this.radius = this.scale.x/2;
        this.isDead = false;
    }
    update(game) {
        this.radius = this.scale.x/2;
    }

    serialize() {
        const buffer = Buffer.alloc(40);

        buffer.writeUInt8(this.networkID, 0);

        buffer.writeFloatBE(this.pos.x, 1);
        buffer.writeFloatBE(this.pos.y, 5);
        buffer.writeFloatBE(this.pos.z, 9);

        buffer.writeFloatBE(this.rotation.x, 13);
        buffer.writeFloatBE(this.rotation.y, 17);
        buffer.writeFloatBE(this.rotation.z, 21);

        buffer.writeFloatBE(this.scale.x, 25);
        buffer.writeFloatBE(this.scale.y, 29);
        buffer.writeFloatBE(this.scale.z, 33);

        buffer.writeUInt8(this.color.r, 37);
        buffer.writeUInt8(this.color.g, 38);
        buffer.writeUInt8(this.color.b, 39);

        return buffer;
    }
}