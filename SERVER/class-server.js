const { Client } = require("./class-client");
const { Game } = require("./class-game");

exports.Server = class Server {


    constructor() {
        this.port = 1111;
        this.serverName = "Journey's Server";
        this.clients = [];
        this.timeUntilNextBroadcast = 5;
        this.maxPlayers = 15;

        this.sock = require("dgram").createSocket("udp4");

        this.sock.on("error", (e) => this.onError(e));
        this.sock.on("listening", () => this.onStartListen());
        this.sock.on("message", (msg, rinfo) => this.onPacket(msg, rinfo));

        this.game = new Game(this);

        this.sock.bind(this.port);
    }
    onError(e) {
        console.log("ERROR: " + e);
    }
    onStartListen() {
        console.log("Now listening on port " + this.port);
    }
    onPacket(msg, rinfo) {
        if (msg.length < 4) return;

        let test = Buffer.from(msg);

        const packetID = msg.slice(0,4).toString();
        const usernameLength = test.readUInt8(5);
        //console.log(usernameLength);

        const c = this.lookupClient(rinfo);
        if(c) {
            c.onPacket(msg, this.game);
        }
        else {
            switch(packetID) {
                case "JOIN":
                    console.log("Someone tried to join from " + rinfo.address);
                    this.makeClient(rinfo, test.slice(5, usernameLength));
                    break;
            }
        }

    }
    disconnectClient(client) {
        if(client.pawn){

        }

        const key = this.getKeyFromRInfo(client.rinfo);

        delete this.clients[key];

        this.showClientsList();

    }

    lookupClient(rinfo) {
        const key = this.getKeyFromRInfo(rinfo);
        return this.clients[key];
    }
    makeClient(rinfo, name) {

        const key = this.getKeyFromRInfo(rinfo);
        //let response = this.checkName(name);
        const client = new Client(rinfo, name);

        this.clients[key] = client;

        /*
        const packet = Buffer.alloc(5);
        packet.write("JOIN");
        packet.writeUInt8(response, 4);
        

        this.sendPacketToClient(packet, client);
        */

        const packet2 = this.game.makeREPL(false);

        this.sendPacketToClient(packet2, client);
        client.spawnPawn(this.game);

        const packet3 = Buffer.alloc(5);
        packet3.write("PAWN");
        packet3.writeUInt8(client.pawn.networkID, 4);
        this.sendPacketToClient(packet3, client);
        /*
        if(response == 1) {
            const client = new Client(rinfo, name);

            this.clients[key] = client;

            const packet = Buffer.alloc(5);
            packet.write("JOIN");
            packet.writeUInt8(response, 4);

            this.sendPacketToClient(packet, client);

            const packet2 = this.game.makeREPL(false);

            this.sendPacketToClient(packet2, client);
            client.spawnPawn(this.game);

            const packet3 = Buffer.alloc(5);
            packet3.write("PAWN");
            packet3.writeUInt8(client.pawn.networkID, 4);
            this.sendPacketToClient(packet3, client);

            console.log("Joined");
        }
        if(response == 2) {
            const client = new Client(rinfo, name);

            this.clients[key] = client;

            const packet = Buffer.alloc(5);
            packet.write("JOIN");
            packet.writeUInt8(response, 4);

            this.sendPacketToClient(packet, client);

            const packet2 = this.makeREPL(false);
            this.sendPacketToClient(packet2, client);
        }
        else {
            const client = new Client(rinfo, name);

            this.clients[key] = client;

            const packet = Buffer.alloc(5);
            packet.write("JOIN");
            packet.writeUInt8(response, 4);

            this.sendPacketToClient(packet, client);

            delete this.clients[key];
        }
        */
        
        

    }
    getKeyFromRInfo(rinfo) {
        return rinfo.address + ":" + rinfo.port;
    }

    getPlayer(num = 0) {
        num = parseInt(num);
        let i = 0;

        for (var key in this.clients) {
            if (num == i) return this.clients[key];

            i++;
        }
    }
    checkName(name) {
        if(name.length < 2) return 4;
        if(name.length > 18) return 3;
        
        const regex1 = /^[a-zA-Z0-9\[\]]+$/;
        if(!regex1.test(name)) return 5;

        let isUsernameTaken = false;
        this.clients.forEach(c=> {
            if(c == client) return;
            if(c.username == name) isUsernameTaken = true;
        });
        if(isUsernameTaken) return 6;
        
        const regex2 = /(fuck|shit|damn|hell|piss|cunt)/i;

        if(regex2.test(name)) return 7;

        
        if(this.clients.length >= this.maxPlayers) return 8;

        // Set users as roles
        if(this.clients.length < 8) return 1;

        return 2;

    }

    showClientsList() {
        console.log(" =========== " + Object.keys(this.clients).length + " clients connected =========== ");
        for(var key in this.clients) {
            console.log(key);
        }
    }

    broadcastPacket(packet) {
        const clientListenPort = 1112;

        this.sock.send(packet, 0, packet.length, clientListenPort, undefined);
    }
    sendPacketToAll(packet) {
        for(var key in this.clients) {
            this.sendPacketToClient(packet, this.clients[key]);
        }
    }
    sendPacketToClient(packet, client) {
        this.sock.send(packet, 0, packet.length, 1112, client.rinfo.address, () => {});
        //console.log(packet.slice(0,4));
    }
    broadcastServerHost() {
        const nameLength = this.serverName.length;
        const packet = Buffer.alloc(7 + nameLength);

        packet.write("HOST", 0);
        packet.writeUInt16BE(this.port, 4);
        packet.writeUInt8(nameLength, 6);
        packet.write(this.serverName, 7);

        this.broadcastPacket(packet);
    }

    update(game) {

        for(let key in this.clients) {
            this.clients[key].update();
        }

        this.timeUntilNextBroadcast -= game.dt;

        if(this.timeUntilNextBroadcast <= 0) {
            this.timeUntilNextBroadcast = 1.5;

            this.broadcastServerHost();
        }


    }
}