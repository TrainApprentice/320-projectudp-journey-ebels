﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetClientInput : MonoBehaviour
{
    

    // Update is called once per frame
    void Update()
    {
        Buffer b = PacketBuilder.CurrInput();

        if(b != null)
        {
            ClientUDP.singleton.SendPacket(b);
            
        }
    }
}
