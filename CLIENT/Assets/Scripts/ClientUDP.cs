﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using System.Net;
using System;

public class ClientUDP : MonoBehaviour
{
    private static ClientUDP _singleton;
    public static ClientUDP singleton
    {
        get { return _singleton; }
        private set { _singleton = value; }
    }


    static UdpClient sockSending;
    static UdpClient sockReceive = new UdpClient(1112);

    public List<RemoteServer> availableServers = new List<RemoteServer>();

    public TMPro.TextMeshProUGUI errorText;

    public Canvas spectatorMode;
    public string username;
    // Start is called before the first frame update
    void Start()
    {
        if(singleton != null)
        {
            Destroy(gameObject);
        }
        else
        {
            singleton = this;
            ListenForPackets();

        }

        
    }

    public void ConnectToServer(string host, ushort port, string name)
    {
        print($"attempt to connect to {host}:{port}");

        IPEndPoint ep = new IPEndPoint(IPAddress.Parse(host), port);
        sockSending = new UdpClient(ep.AddressFamily);

        sockSending.Connect(ep);

        username = name;

        Buffer packet = Buffer.Alloc(5 + name.Length);
        packet.WriteString("JOIN");
        packet.WriteUInt8((byte)name.Length, 4);
        packet.WriteString(name, 5);

        print(name);

        SendPacket(packet);

    }

    private async void ListenForPackets()
    {
        while(true)
        {
            UdpReceiveResult result;
            try
            {
                result = await sockReceive.ReceiveAsync();
                

                ProcessPacket(result);
            }
            catch (Exception e)
            {
                print(e);
                break;
            }
        }
    }

    private void ProcessPacket(UdpReceiveResult res)
    {
        Buffer packet = Buffer.From(res.Buffer);
        if (packet.Length < 4) return;
        string id = packet.ReadString(0, 4);
        //print(id);
        switch(id)
        {
            case "JOIN":
                if (packet.Length < 5) return;

                int response = packet.ReadUInt8(4);

                if (response > 2) SetError(response);
                else if(response == 2)
                {
                    spectatorMode.gameObject.SetActive(true);
                }
                

                break;

            case "HOST":
                //print(packet.Length);
                if (packet.Length < 7) return;
                ushort port = packet.ReadUInt16BE(4);
                int nameLength = packet.ReadUInt8(6);

                if (packet.Length < 7 + nameLength) return;

                string name = packet.ReadString(7, nameLength);


                AddToServerList(new RemoteServer(res.RemoteEndPoint, name));

                break;

            case "REPL":
                ProcessPacketREPL(packet);
                break;
            case "PAWN":
                
                if (packet.Length < 5) return;

                byte netID = packet.ReadUInt8(4);
                
                NetworkObject obj = NetworkObject.GetObjectByNetID(netID);
                if (obj)
                {
                    Pawn p = (Pawn)obj;
                    if (p != null) p.canPlayerControl = true;
                }
                break;
            default:
                print(id);
                
                break;
        }
    }

    private void ProcessPacketREPL(Buffer packet)
    {
        if (packet.Length < 5) return;
        int replType = packet.ReadUInt8(4);
        int offset = 5;

        if (replType < 1 || replType > 3) return;

        while(offset <= packet.Length)
        {
            int netID = 0;
            switch(replType)
            {
                case 1: // Create
                    if (packet.Length < offset + 5) return;

                    netID = packet.ReadUInt8(offset + 4);

                    string classID = packet.ReadString(offset, 4);

                    if (NetworkObject.GetObjectByNetID(netID) != null) return;

                    NetworkObject obj = ObjectRegistry.SpawnFrom(classID);

                    if (obj == null) return;

                    offset += 4;
                    offset += obj.Deserialize(packet.Slice(offset));

                    NetworkObject.AddObject(obj);

                    break;
                case 2: // Update

                    if (packet.Length < offset + 5) return;
                    netID = packet.ReadUInt8(offset + 4);

                    NetworkObject obj2 = NetworkObject.GetObjectByNetID(netID);
                    if (obj2 == null) return;

                    offset += 4;
                    offset += obj2.Deserialize(packet.Slice(offset));
                    break;
                case 3: // Delete
                    if (packet.Length < offset + 1) return;
                    netID = packet.ReadUInt8(offset);

                    NetworkObject obj3 = NetworkObject.GetObjectByNetID(netID);
                    if (obj3 == null) return;
                    NetworkObject.RemoveObject(netID);
                    Destroy(obj3.gameObject);
                    offset++;

                    break;
            }
        }
    }

    private void SetError(int num)
    {

    }
    public async void SendPacket(Buffer packet)
    {
        if (sockSending == null) return;
        if (!sockSending.Client.Connected) return;

        await sockSending.SendAsync(packet.Bytes, packet.Bytes.Length);
    }

    private void OnDestroy()
    {
        if (sockSending != null) sockSending.Close();
        if (sockReceive != null) sockReceive.Close();
    }

    private void AddToServerList(RemoteServer server)
    {
        if(!availableServers.Contains(server))
        {
            availableServers.Add(server);
        }
    }
}
