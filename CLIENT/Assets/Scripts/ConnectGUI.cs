﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ConnectGUI : MonoBehaviour
{

    public ServerRowGUI prefab;
    public Button directConnect, connectButton, submitButton;

    public Canvas directCanvas, usernameCanvas;

    public TMP_InputField hostInput, portInput, usernameInput;
    private List<ServerRowGUI> rows = new List<ServerRowGUI>();
    public RemoteServer holding;

    private string directAddress;
    private ushort directPort;

    float refreshTimer = 1f;

    private void Start()
    {
        directConnect.onClick.AddListener(OnDirectClicked);
        connectButton.onClick.AddListener(OnConnectClicked);
        submitButton.onClick.AddListener(OnSubmitClicked);
    }
    private void Update()
    {
        refreshTimer -= Time.deltaTime;

        if(refreshTimer <= 0)
        {
            refreshTimer = 2f;
            if(directConnect.gameObject.activeInHierarchy) UpdateServerList();
        }
    }

    public void UpdateServerList()
    {

        foreach(ServerRowGUI row in rows)
        {
            if (row != null) Destroy(row.gameObject);
        }
        rows.Clear();

        int i = 0;
        foreach (RemoteServer s in ClientUDP.singleton.availableServers)
        {
            ServerRowGUI row = Instantiate(prefab, transform);

            RectTransform rect = (RectTransform)row.transform;
            rect.localScale = Vector3.one;
            rect.sizeDelta = new Vector2(500, 50);
            rect.anchorMax = rect.anchorMin = new Vector2(.5f, .5f);

            rect.anchoredPosition = new Vector2(0, -i * 70);
            row.Init(s);
            rows.Add(row);
            i++;
        }
    }

    public void TryConnect(RemoteServer server)
    {
        ClientUDP.singleton.ConnectToServer(server.endPoint.Address.ToString(), (ushort)server.endPoint.Port, usernameInput.text);
        
    }

    public void TryConnect(string address, ushort port)
    {
        ClientUDP.singleton.ConnectToServer(address, port, usernameInput.text);
        print("Address: " + address);
        print("Port: " + port);
    }

    public void OnDirectClicked()
    {
        directCanvas.gameObject.SetActive(true);
        directConnect.gameObject.SetActive(false);
        foreach (ServerRowGUI row in rows)
        {
            if (row != null) Destroy(row.gameObject);
        }
        rows.Clear();
    }

    public void OnConnectClicked()
    {
        usernameCanvas.gameObject.SetActive(true);
        directCanvas.gameObject.SetActive(false);

        directAddress = hostInput.text;
        UInt16.TryParse(portInput.text, out directPort);
        
    }

    public void OnSubmitClicked()
    {
        if (holding != null) TryConnect(holding);
        else TryConnect(directAddress, directPort);
        gameObject.SetActive(false);
    }


}
