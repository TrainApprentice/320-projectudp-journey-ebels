﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PacketBuilder 
{

    static int prevInputH = 0;
    static int prevInputV = 0;
    public static Buffer CurrInput()
    {
        int h = (int)Input.GetAxisRaw("Horizontal");
        int v = (int)Input.GetAxisRaw("Vertical");

        //if (h == prevInputH) return null;
        //if (v == prevInputV) return null;

        prevInputH = h;
        prevInputV = v;

        Buffer b = Buffer.Alloc(6);
        b.WriteString("INPT", 0);
        b.WriteInt8((sbyte)h, 4);
        b.WriteInt8((sbyte)v, 5);

        return b;
    }
}
