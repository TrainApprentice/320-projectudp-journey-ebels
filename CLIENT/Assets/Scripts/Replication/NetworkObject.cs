﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkObject : MonoBehaviour
{
    public int networkID;
    public static string classID = "NWOB";
    public Material mat;

    private static Dictionary<int, NetworkObject> currObjects = new Dictionary<int, NetworkObject>();

    public static void AddObject(NetworkObject obj)
    {
        if(!currObjects.ContainsKey(obj.networkID))
        {
            currObjects.Add(obj.networkID, obj);
        }
    }

    public static void RemoveObject(int netID)
    {
        if (currObjects.ContainsKey(netID))
        {
            currObjects.Remove(netID);
        }
    }

    public static void RemoveObject(NetworkObject obj)
    {
        RemoveObject(obj.networkID);
    }

    public static NetworkObject GetObjectByNetID(int netID)
    {
        
        if (!currObjects.ContainsKey(netID)) return null;

        return currObjects[netID];
    }

    public virtual int Deserialize(Buffer packet)
    {
        networkID = packet.ReadUInt8(0);

        float px = packet.ReadSingleLE(1);
        float py = packet.ReadSingleLE(5);
        float pz = packet.ReadSingleLE(9);
                                    
        float rx = packet.ReadSingleLE(13);
        float ry = packet.ReadSingleLE(17);
        float rz = packet.ReadSingleLE(21);
                                    
        float sx = packet.ReadSingleLE(25);
        float sy = packet.ReadSingleLE(29);
        float sz = packet.ReadSingleLE(33);

        float cr = packet.ReadUInt8(37);
        float cg = packet.ReadUInt8(38);
        float cb = packet.ReadUInt8(39);

        transform.position = new Vector3(px, py, pz);
        transform.rotation = Quaternion.Euler(rx, ry, rz);
        transform.localScale = new Vector3(sx, sy, sz);

        
        //mat.SetColor("_BaseColor", new Color(cr, cg, cb));
        return 40;
        
    }
}
