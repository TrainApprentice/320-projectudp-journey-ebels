﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : NetworkObject
{
    new public static string classID = "PAWN";

    public bool canPlayerControl = false;
    private Camera cam;
    private int speed = 5;
    public TextMesh name;

    //Vector3 vel = new Vector3();

    void FixedUpdate()
    {
        if(!cam) cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        if (ClientUDP.singleton.username != null) name.text = ClientUDP.singleton.username;
        if (canPlayerControl)
        {
            int moveX = (int)Input.GetAxisRaw("Horizontal") * speed;
            int moveY = (int)Input.GetAxisRaw("Vertical") * speed;

            Vector3 pos = transform.position;
          
            pos += new Vector3(moveX, moveY, 0) * Time.fixedDeltaTime;
            //print(pos);
            transform.position = pos;
            name.transform.localPosition = new Vector3(name.transform.localPosition.x, name.transform.localPosition.y, -transform.localScale.z / 2);
            name.transform.localScale = new Vector3(1 / transform.localScale.x, 1 / transform.localScale.y, 1/transform.localScale.z);
            cam.transform.position = new Vector3(transform.position.x, transform.position.y, -10 - transform.localScale.z * 2);
        }
    }
    /*
    float Accelerate(float vel, float acc)
    {
        if (acc != 0)
        {
            vel += acc * Time.fixedDeltaTime;
        }
        else
        {
            if (vel > 0)
            {
                acc = -1;
                vel += acc * Time.fixedDeltaTime;
                if (vel < 0) vel = 0;
            }
            if (vel < 0)
            {
                acc = 1;
                vel += acc * Time.fixedDeltaTime;
                if (vel > 0) vel = 0;
            }
        }
        return vel;
    }
    */
    public override int Deserialize(Buffer packet)
    {
        return base.Deserialize(packet);
    }
}
