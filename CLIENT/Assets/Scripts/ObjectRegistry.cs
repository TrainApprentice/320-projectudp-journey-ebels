﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRegistry : MonoBehaviour
{
    [Serializable]
    public class RegisteredPrefab
    {
        public string classID = "";
        public NetworkObject prefab;
    }

    private static ObjectRegistry _singleton;

    private static Dictionary<string, NetworkObject> registeredPrefabs = new Dictionary<string, NetworkObject>();

    public RegisteredPrefab[] prefabs;

    public void Start()
    {
        if(_singleton == null)
        {
            _singleton = this;
            DontDestroyOnLoad(gameObject);
            RegisterAll();
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void RegisterAll()
    {
        foreach(var rp in prefabs)
        {
            if(registeredPrefabs.ContainsKey(rp.classID) == false)
            {
                registeredPrefabs.Add(rp.classID, rp.prefab);
            }
        }
    }

    public static NetworkObject SpawnFrom(string classID)
    {
        if(registeredPrefabs.ContainsKey(classID)) {
            return Instantiate(registeredPrefabs[classID]);
        }
        return null;
        
    }
}
