﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerRowGUI : MonoBehaviour
{
    public Text displayName;
    public Button button;

    private RemoteServer server;
    private ConnectGUI owner;

    public void Init(RemoteServer s)
    { 
        server = s;

        displayName.text = s.serverName;

        owner = GameObject.Find("Canvas").GetComponent<ConnectGUI>();

        button.onClick.AddListener(OnJoinClicked);
    }

    public void OnJoinClicked()
    {
        //ClientUDP.singleton.ConnectToServer(server.endPoint.Address.ToString(), (ushort)server.endPoint.Port);
        owner.usernameCanvas.gameObject.SetActive(true);
        owner.directConnect.gameObject.SetActive(false);
        gameObject.SetActive(false);
        owner.holding = server;

    }
    
}
